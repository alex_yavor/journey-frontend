import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JourneysComponent } from '../components/journeys/journeys.component';
import { JourneyDetailComponent } from '../components/journey-detail/journey-detail.component';
import { RegisterComponent } from '../components/register/register.component';
import { LoginComponent } from '../components/login/login.component';
import { AuthGuard } from '../guards/auth.guard';
import { CreateJourneyComponent, UserProfileComponent } from '../components';
import { ProfileComponent } from '../components/profile/profile.component';

const routes: Routes = [
  { path: 'journeys', component: JourneysComponent },
  { path: 'journeys/:id', component: JourneyDetailComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'profile/:id', component: UserProfileComponent },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'createJourney', component: CreateJourneyComponent, canActivate: [AuthGuard] },
  { path: '', redirectTo: 'profile', canActivate: [AuthGuard], pathMatch: 'full' },
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
