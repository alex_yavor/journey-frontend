import { Component, OnInit } from '@angular/core';
import { AuthService, JourneyService } from '../../services';
import { Router } from '@angular/router';
import { ReturnStatement } from '@angular/compiler';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private journeyService: JourneyService,
    private route: Router
  ) { }

  get isLoggedIn(): boolean { return this.authService.isLoggedIn; }

  ngOnInit() {
  }


  profileButtons = [
    { name: 'Поиск поездок', route: 'journeys' },
    { name: 'Создать поездку', route: 'createJourney' },
    { name: 'Профиль', route: 'profile' },
  ]


  authButtons = [
    { name: 'Поиск поездок', route: 'journeys' },
    { name: 'Логин', route: 'login' },
    { name: 'Регистрация', route: 'register' },
  ]


  logout(): void {
    this.authService.logout();
    this.route.navigate(['login'])
  }

}
