import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../services';

@Component({
  selector: 'app-upcomig-journeys',
  templateUrl: './upcomig-journeys.component.html',
  styleUrls: ['./upcomig-journeys.component.css']
})
export class UpcomigJourneysComponent implements OnInit {

  constructor(
    private profileService: ProfileService
  ) { }

  journeys;

  getJourneys(): void {
    this.profileService.upcomingJourneys()
      .subscribe(res => {
        this.journeys = Object.values(res.journeys);
        this.journeys[0].name = 'Как попутчик: ';
        this.journeys[1].name = 'Как водитель: ';
      })

  }

  ngOnInit() {
    this.getJourneys();
  }

}
