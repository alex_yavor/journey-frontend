import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    public snackBar: MatSnackBar
  ) { }

  hide: boolean = true;
  loginForm: FormGroup;

  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }

  ngOnInit() {
    this.loginForm = this.fb.group({
      'email': [null, Validators.compose([Validators.required, Validators.email])],
      'password': [null, Validators.required]
    })
  }

  loginUser(post) {

    let email = post.email;
    let password = post.password;

    this.authService.loginUser({ email, password }).subscribe(res => {
      if (!res['success'])
        this.snackBar.open(res['message'], null, {
          duration: 3000
        })
      else
        this.router.navigate(['profile']);
    });
  }
}
