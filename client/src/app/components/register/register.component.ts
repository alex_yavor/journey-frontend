import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Genders, Years } from '../../interfaces/formsVariables';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    public snackBar: MatSnackBar
  ) { }

  
  get email() { return this.registerForm.get('email'); }
  get password() { return this.registerForm.get('password'); }
  get confirmPassword() { return this.registerForm.get('confirmPassword'); }

  public genders = Genders;
  public years = Years;
  public registerForm: FormGroup;
  public hide: boolean = true;


  registerUser(post) {

    delete post.confirmPassword;
    this.authService.registerUser(post).subscribe(res => {
      this.snackBar.open(res.message, null, {
        duration: 3000
      })
      if (res.success === true)
        this.router.navigate(['journeys']);
    });
  }

  private checkPassowrd(group: FormGroup) {
    let password = group.controls.password.value;
    let confirmPassword = group.controls.confirmPassword.value;
    if (password !== confirmPassword)
      return group.controls.confirmPassword.setErrors({ notSame: true });
    else return group.controls.confirmPassword.setErrors(null);
  }


  ngOnInit() {
    this.registerForm = this.fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.email])],
      'password': ['', Validators.required],
      'confirmPassword': ['', Validators.required],
      'name': ['', Validators.required],
      'surname': ['', Validators.required],
      'age': ['', Validators.required],
      'gender': ['', Validators.required],
      'phone': ['', Validators.required]
    }, { validator: this.checkPassowrd })
  }

}
