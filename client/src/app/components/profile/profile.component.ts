import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileService } from '../../services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { Genders, Years } from '../../interfaces/formsVariables';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(
    private profileService: ProfileService,
    private formBuilder: FormBuilder,
    public snackBar: MatSnackBar
  ) { }

  profileGroup: FormGroup;
  autoGroup: FormGroup;
  private state = 'profile';

  get upcoming(): boolean { return this.state === 'upcoming' }
  get completed(): boolean { return this.state == 'completed' }
  get edit(): boolean { return this.state == 'edit' }
  get editAuto(): boolean { return this.state == 'editAuto' }
  get profile(): boolean { return this.state == 'profile' }
  public genders = Genders;
  public years = Years;


  // getProfile(): void {

  //   this.profileService.getMyProfile()
  //     .subscribe(res => console.log(res));


  // }


  showJourneys(value): void {
    this.state = value;
  }


  showMyProfile() {
    this.state = 'profile'
    this.profileService.getMyProfile()
      .subscribe(res => {
        console.log('ssss', res);
        res.info.birthday = new Date(res.info.birthday).getFullYear();
        this.profileGroup.setValue(res.info);
        if (res.auto)
          this.autoGroup.setValue(res.auto)
      })
  }


  saveMyInfo(info) {
    console.log(info);
    this.profileService.saveMyInfo(info)
      .subscribe(res => {
        this.snackBar.open(res.message, null, {
          duration: 2000
        })
        setTimeout(() => this.state = 'profile', 1000)
      })

  }
  saveMyAuto(info) {
    console.log(info);
    this.profileService.saveMyAuto(info)
      .subscribe(res => {
        this.snackBar.open(res.message, null, {
          duration: 2000
        })
        setTimeout(() => this.state = 'profile', 1000)
      })

  }

  ngOnInit() {


    //  this.getProfile();
    this.showMyProfile();
    this.profileGroup = this.formBuilder.group({
      'gender': [null, Validators.required],
      'name': [null, Validators.required],
      'surname': [null, Validators.required],
      'email': [null, Validators.required],
      'phone': [null, Validators.required],
      'birthday': [null, Validators.required],
      'social': [null,],
      'info': [null,],
    })
    this.autoGroup = this.formBuilder.group({
      'year': [null, Validators.required],
      'mark': [null, Validators.required],
      'model': [null, Validators.required],
      'numberplate': [null, Validators.required],
      'typeauto': [null, Validators.required],
      'color': [null, Validators.required],

    })
  }

}
