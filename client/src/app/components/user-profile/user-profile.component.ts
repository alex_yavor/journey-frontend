import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProfileService } from '../../services';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, Router } from '@angular/router';
import { profile, feedback } from '../../interfaces';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  @Input() public profile:profile;
  @Input() public feedbacks:feedback[];
  @Input() public editProfile: boolean;
  @Output() showEditForm = new EventEmitter<string>();
  public age: string;
  public editAuto: string;
  showEditFormClick(view: string) {
    this.showEditForm.emit(view);
  }

  ngOnInit() {

    this.showUserProfile();
  }

  showUserProfile() {
    const userId = arguments[0] || +this.route.snapshot.paramMap.get('id');
    this.profileService.getUserProfile(userId)
      .subscribe(res => {
        if (res == null) {
          this.router.navigate(['profile']);
          return;
        }
        this.profile = res.info;
        this.feedbacks = res.feedbacks;

        if (this.profile.dage % 10 < 5 && this.profile.dage % 10 > 1)
          this.age = 'года';
        else if (this.profile.dage % 10 == 1)
          this.age = 'год';
        else this.age = 'лет';

        if (this.profile.markauto)
          this.editAuto = 'Изменить данные'
        else this.editAuto = 'Добавить машину'


        console.log(res)
      });
  }

}
