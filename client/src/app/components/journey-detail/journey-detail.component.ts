import { Component, OnInit, Input, Inject } from '@angular/core';
import { JourneyService } from '../../services/journey.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AuthService } from '../../services';
import { passanger, journeyDetail } from '../../interfaces';

@Component({
  selector: 'app-journey-detail',
  templateUrl: './journey-detail.component.html',
  styleUrls: ['./journey-detail.component.css']
})
export class JourneyDetailComponent implements OnInit {
 
  constructor(

    public snackBar: MatSnackBar,
    public dialog: MatDialog,

    private router: Router,
    private journeyService: JourneyService,
    private route: ActivatedRoute,
    private authService:AuthService

  ) { }

  public journeyDetail:journeyDetail;
  public passangers:passanger[];
  private role = null;
  get isCompleted() { return this.journeyDetail.iscompleted || new Date(this.journeyDetail.arrival) <= new Date() };
  get isCompnaion() { return this.role === 'companion' };
  get isDriver() { return this.role === 'driver' };
  get isInProcess() { return new Date(this.journeyDetail.arrival) > new Date() && new Date(this.journeyDetail.departure) <= new Date() };
  get isLoggedIn(): boolean { return this.authService.isLoggedIn; }



  getJourney(id: number = +this.route.snapshot.paramMap.get('id')): void {
    this.journeyService.getJourney(id).subscribe(info => {
      this.journeyDetail = info.trip;
      console.log(this.journeyDetail)
      this.passangers = info.passangers;
      this.role = info.role;
      console.log(this.passangers)
    })
  }


  ngOnInit() {
    this.getJourney();
  }


  bookPlace() {
    const journeyId = +this.route.snapshot.paramMap.get('id');
    this.journeyService.bookPlace(journeyId)
      .subscribe(res => {
        let action = null;
        let journeyId = res.message.split(' ');
        journeyId = journeyId[journeyId.length - 1];
        console.log('jjj', journeyId)
        if (res.success === true) {
          this.passangers.push(res.userInfo)
          this.role = 'companion';
          this.journeyDetail.currseats--;
        }
        else {
          action = 'Просмотреть поездку';


        }
        this.snackBar.open(res.message, action, {
          duration: 2000
        })
        this.snackBar._openedSnackBarRef.onAction().subscribe(() => {
          this.router.navigate([`journeys/${journeyId}`]);
          this.getJourney(journeyId);

        });
        console.log(res);

      });
  }

  completeJourney() {
    const journeyId = +this.route.snapshot.paramMap.get('id');
    this.journeyService.completeJourney(journeyId)
      .subscribe(res => console.log(res))
  }


  deleteJourney() {
    const dialog = this.dialog.open(DeleteJourneyDialog, {
      height: '180px',
      data: { name: 'Удалить' }
    })
    dialog.afterClosed()
      .subscribe(result => {

        if (result) {
          const journeyId = +this.route.snapshot.paramMap.get('id');
          this.journeyService.deleteJourney(journeyId)
            .subscribe(res => console.log(res))
        }
      })
  }

  cancelJourney() {
    const dialog = this.dialog.open(DeleteJourneyDialog, {
      height: '180px',
      data: { name: 'Отменить' }
    })
    dialog.afterClosed()
      .subscribe(result => {

        if (result) {
          const journeyId = +this.route.snapshot.paramMap.get('id');
          this.journeyService.cancelJourney(journeyId)
            .subscribe(res => {
              this.snackBar.open(res.message, null, {
                duration: 2000
              })
              if (res.success === true) {
                this.passangers = this.passangers.filter(passanger => passanger.userid != res.userId);
                this.role = null;
                this.journeyDetail.currseats++
              }
              console.log(res)
            })
        }
      })

  }


  leaveCommentToDriver(userId: number) {
    this.leaveComment(userId, 'companion');
  }


  leaveCommentToCompanion(userId: number) {
    this.leaveComment(userId, 'driver');
  }

  leaveComment(userId: number, role: string) {
    const dialog = this.dialog.open(LeaveFeedbackDialog, {
      minHeight: '300px',
      width: '400px',
    })
    dialog.afterClosed()
      .subscribe(result => {


        if (result) {
          console.log(result);
          result.userId = userId;
          result.role = role;
          this.journeyService.leaveFeedback(result)
            .subscribe(res => {

              this.snackBar.open(res.message, null, {
                duration: 2000
              })

              console.log(res)
            })
        }
      })
  }


  cancelBooking(userId) {
    const journeyId = +this.route.snapshot.paramMap.get('id');
    this.journeyService.cancelBooking(journeyId, userId)
      .subscribe(res => {
        if (res.success === true) {
          this.passangers = this.passangers.filter(passanger => passanger.userid != userId);
        }
        this.snackBar.open(res.message, null, {
          duration: 2000
        })
      })
  }

}


@Component({
  selector: 'app-delete-journey',
  templateUrl: './delete.journey.dialog.html',

})


export class DeleteJourneyDialog {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any) { }
}


@Component({
  selector: 'app-leave-feedback',
  templateUrl: './feedback.html',
  styleUrls: ['./feedback.css']

})
export class LeaveFeedbackDialog {
  marks = Array.from(Array(10).keys());

}

