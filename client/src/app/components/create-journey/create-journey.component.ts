import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Validators, FormArray, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { JourneyService } from '../../services';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import { } from 'googlemaps';

@Component({
  selector: 'app-create-journey',
  templateUrl: './create-journey.component.html',
  styleUrls: ['./create-journey.component.css']
})
export class CreateJourneyComponent implements OnInit {



  @ViewChild("departurePoint")
  public departurePointElement: ElementRef;
  @ViewChild("arrivalPoint")
  public arrivalPointElement: ElementRef;


  constructor(
    private formBuilder: FormBuilder,
    private journeyService: JourneyService,
    public snackBar: MatSnackBar,
    private router: Router,
    private ngZone: NgZone,
    private mapsAPILoader: MapsAPILoader,
  ) { }

  journeyFormGroup: FormGroup;
  detailFormGroup: FormGroup;
  minDate = new Date();
  canCreateJourney: boolean = true;
  private arrivalCity: string = " ";
  private departureCity: string = " ";
  private arrivalStreet: string = " ";
  private departureStreet: string = " ";

  markers = [
    { lat: null, lng: null },
    { lat: null, lng: null }
  ]
  public directionService;
  public direction = [];
  public latitude: number;
  public longitude: number;
  public zoom: number;
  public journeyTime;

  ngOnInit() {

    this.detailFormGroup = this.formBuilder.group({
      'places': [null, Validators.required],
      'price': [null, Validators.required],
      // 'info': [null]
    })
    this.journeyFormGroup = this.formBuilder.group({
      'departurePoint': [null, Validators.required],
      'arrivalPoint': [null, Validators.required],
      'departure': [null, Validators.required],
      'arrival': [null, Validators.required],
      'arrivalTime': [null, Validators.required],
      'departureTime': [null, Validators.required],
    })
    this.zoom = 4;
    this.setCurrentPosition();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      let departurePoint = new google.maps.places.Autocomplete(this.departurePointElement.nativeElement);
      let arrivalPoint = new google.maps.places.Autocomplete(this.arrivalPointElement.nativeElement);

      departurePoint.addListener("place_changed", () =>
        this.getPlace(
          departurePoint,
          'departurePoint',
          'departureCity',
          'departureStreet',
          this.markers[0]
        ));

      arrivalPoint.addListener("place_changed", () =>
        this.getPlace(
          arrivalPoint,
          'arrivalPoint',
          'arrivalCity',
          'arrivalStreet',
          this.markers[1]
        ));
    });
  }

  getPlace(Point, PointName, City, Street, Marker) {

    this.ngZone.run(() => {
      let place: google.maps.places.PlaceResult = Point.getPlace();
      console.log(place);
      let street = place.address_components.filter((component) => component.types[0] == 'route')
      if (!street[0])
        street = place.address_components.filter((component) => component.types[0] == "neighborhood");

      if (!street[0]) {
        // this.journeyFormGroup.get(PointName).setErrors({ e: 'Укажите точный адресс' })
        this[Street] = place.name;
        this.snackBar.open('Укажите по возможности более точный адресс', null, {
          duration: 2000,
        });
      }
      else
        this[Street] = street[0].long_name;

      let depPlace = place.address_components.filter((component) => component.types[0] == 'locality');

      if (!depPlace[0])
        this.journeyFormGroup.get(PointName).setErrors({ e: 'Укажите город' })
      else
        this[City] = depPlace[0].long_name;
      if (this[City] == this[Street])
        this[Street] = ''
      console.log(street)
      if (place.geometry === undefined || place.geometry === null) {
        this[City] = Street = '';
        return;
      }

      Marker.lat = place.geometry.location.lat();
      Marker.lng = place.geometry.location.lng();
      this.latitude = Marker.lat;
      this.longitude = Marker.lng;
      this.showDirection();
      this.zoom = 12;
    });
  }


  createJourney() {
    this.canCreateJourney = false;
    //let detail = this.detailFormGroup.get(['places', 'price', 'info']);
    let numofseats = this.detailFormGroup.get('places').value;
    let price = this.detailFormGroup.get('price').value;
    // let info = this.detailFormGroup.get('info').value;
    let departureTime = this.journeyFormGroup.get('departureTime').value;
    let arrivalTime = this.journeyFormGroup.get('arrivalTime').value;
    let departure = this.journeyFormGroup.get('departure').value;
    let arrival = this.journeyFormGroup.get('arrival').value;
    let journeyDetail = {
      departure, arrival,
      numofseats, price, 
      arrivalTime, departureTime,
      arrivalCity: this.arrivalCity,
      departureCity: this.departureCity,
      departureStreet: this.departureStreet,
      arrivalStreet: this.arrivalStreet
    };
    console.log('sdsdsdsd', journeyDetail)
    this.journeyService.createJourney(journeyDetail)
      .subscribe(res => {
        this.snackBar.open(res.message, null, {
          duration: 2000,
        });
        if (res.success !== true)
          this.canCreateJourney = true;
        console.log(res);
      });
  }

  private setCurrentPosition() {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
      });
    }
  }

  showDirection() {
    if (!this.directionService)
      this.directionService = new google.maps.DirectionsService();

    if (this.markers[0].lat && this.markers[1].lat) {
      let scale = new Proxy([], {
        get: (target, index) => 1128.497220 * 2 ** (20 - Number(index)),

      })

      this.directionService.route({
        origin: this.markers[0],
        destination: this.markers[1],
        travelMode: google.maps.TravelMode.DRIVING
      }, (result, response) => {
        console.log(response);
        if (response === google.maps.DirectionsStatus.OK) {
          let distance = result.routes[0].legs[0].distance.value;
          this.journeyTime = result.routes[0].legs[0].duration.value;
          let zoom = 0;
          while (zoom <= 20) {
            if (distance < scale[zoom])
              zoom++;
            else break;
          }

          this.zoom = Math.ceil(zoom / 2);
          this.latitude = (this.markers[0].lat + this.markers[1].lat) / 2;
          this.longitude = (this.markers[0].lng + this.markers[1].lng) / 2;
          console.log(result)
          this.direction = [];
          result.routes[0].overview_path.forEach((path) => {
            this.direction.push({ lat: path.lat(), lng: path.lng() })
          })
        }
      })
    }

  }


  offerTime() {
    if (!this.journeyTime)
      return;
    let departureTime = this.journeyFormGroup.get('departureTime').value;
    let departure = this.journeyFormGroup.get('departure').value;
    if (!departure || !departureTime)
      return;
    departureTime = departureTime.split(':')

    let depTime = new Date(departure);
    depTime.setHours(departureTime[0]);
    depTime.setMinutes(departureTime[1]);

    let arrTime = new Date(Number(depTime) + this.journeyTime * 1000);
    let hours = arrTime.getHours() < 10 ? '0' + arrTime.getHours() : arrTime.getHours()
    let minutes = arrTime.getMinutes() < 10 ? '0' + arrTime.getMinutes() : arrTime.getMinutes()
    let arrivalTime = `${hours}:${minutes}`;
    this.journeyFormGroup.get('arrivalTime').setValue(arrivalTime);

    this.journeyFormGroup.get('arrival').setValue(arrTime);
    console.log('time', arrTime, arrivalTime)


  }

}

