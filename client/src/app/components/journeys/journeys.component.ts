import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { JourneyService } from '../../services/journey.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { MapsAPILoader } from '@agm/core';
import { journey } from '../../interfaces';

@Component({
  selector: 'app-journeys',
  templateUrl: './journeys.component.html',
  styleUrls: ['./journeys.component.css']
})
export class JourneysComponent implements OnInit {

  journeys:journey[];
  constructor(
    private journeyService: JourneyService,
    private fb: FormBuilder,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
  ) { }

  searchForm: FormGroup;
  popularTrips = [];

  @ViewChild("departurePoint")
  public departurePointElement: ElementRef;
  @ViewChild("arrivalPoint")
  public arrivalPointElement: ElementRef;



  ngOnInit() {
    this.getPopularTrips();
    this.searchForm = this.fb.group({
      'departure': new FormControl(),
      'departurePoint': new FormControl(),
      'arrivalPoint': new FormControl(),
    })


    this.mapsAPILoader.load().then(() => {
      let departurePoint = new google.maps.places.Autocomplete(this.departurePointElement.nativeElement, { types: ['(cities)'] });
      let arrivalPoint = new google.maps.places.Autocomplete(this.arrivalPointElement.nativeElement, { types: ['(cities)'] });

      departurePoint.addListener("place_changed", () =>
        this.getPlace(
          departurePoint,
          'departurePoint',
        ));

      arrivalPoint.addListener("place_changed", () =>
        this.getPlace(
          arrivalPoint,
          'arrivalPoint',
        ));

    })
  }
  getPlace(Point, PointName) {

    this.ngZone.run(() => {
     
      let place: google.maps.places.PlaceResult = Point.getPlace();
      let depPlace =  Point.getPlace().address_components.filter((component) => component.types[0] == 'locality');

      if (depPlace[0])
        this.searchForm.get(PointName).setValue(depPlace[0].long_name);

      if (place.geometry === undefined || place.geometry === null) {
        return;
      }
    });
  }


  minDate = new Date();

  // get cols() {
  //   let cols = 2;
  //   window.innerWidth > 800 ? cols = 2 : cols = 1;
  //   return cols;
  // }

  findJourneys(post) {
    console.log('post', post)
    this.journeyService.getJourneys(post)
      .subscribe(journeys => {
        this.journeys = journeys;
        console.log(this.journeys)

      })
  }


  findPopularJourneys(trip) {
    let post = { departurePoint: trip.departurepoint, arrivalPoint: trip.arrivalpoint, departure: null }
    this.searchForm.setValue(post);
    console.log(this.searchForm.value)
    this.findJourneys(post);
  }


  getPopularTrips() {
    this.journeyService.getPopularTrips()
      .subscribe(res => this.popularTrips = res)
  }


}
