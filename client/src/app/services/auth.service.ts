import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/shareReplay';
import * as moment from 'moment';
export interface IAuthInfo {
    success: boolean;
    message: string;
}

@Injectable()
export class AuthService {

    constructor(
        private http: HttpClient,
    ) { }


    registerUser(info): Observable<IAuthInfo> {
        return this.http.post<IAuthInfo>('api/register', info)
            .do(res => { this.setSession(res) })
            .shareReplay();
    }


    loginUser(info) {
        return this.http.post('api/login', info)
            .do(res => { this.setSession(res) })
            .shareReplay();
    }


    logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
    }


    private setSession(authResult) {
        const expiresAt = moment().add(authResult.expiresIn, 'second');
        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    }


    getExpiration() {
        const expiration = localStorage.getItem("expires_at");
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }


    get isLoggedIn() {
        if (moment().isBefore(this.getExpiration()) && Boolean(localStorage.getItem('id_token')))
            return true;
        else {
            this.logout();
            return false;
        }
    }


}

