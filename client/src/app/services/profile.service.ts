import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ProfileService {

    constructor(
        private http: HttpClient) { }


    getMyProfile(): Observable<any> {
        return this.http.get('api/profile');
    }

    upcomingJourneys(): Observable<any> {
        return this.http.get('api/profile/upcoming');
    }

    completedJourneys(): Observable<any> {
        return this.http.get('api/profile/completed');
    }

    getUserProfile(userId: number): Observable<any> {
        return this.http.get('api/profile/' + userId)
    }

    saveMyInfo(info): Observable<any> {
        return this.http.post('api/profile', info)
    }
    
    saveMyAuto(info): Observable<any> {
        return this.http.post('api/profile/car', info)
    }

}