import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class JourneyService {

  constructor(
    private http: HttpClient) { }

  getJourneys(post): Observable<any> {
    let query = '/api/journey?';
    for (let param in post)
      query += param + '=' + post[param] + '&';
    query = query.slice(0, query.length - 1);
    return this.http.get(query);
  }

  getJourney(id: number): Observable<any> {
    return this.http.get('api/journey/' + id)
  }

  bookPlace(journeyId: number): Observable<any> {
    return this.http.post('api/journey/' + journeyId, {})
  }

  createJourney(journeyDetail): Observable<any> {
    return this.http.put('api/journey', journeyDetail);
  }

  completeJourney(journeyId: number): Observable<any> {
    return this.http.patch('api/journey/' + journeyId, {})
  }

  deleteJourney(journeyId: number): Observable<any> {
    return this.http.delete('api/journey/' + journeyId)
  }

  cancelJourney(journeyId: number): Observable<any> {
    return this.http.get(`api/journey/${journeyId}/cancel`);
  }


  leaveFeedback(feedback: object): Observable<any> {
    return this.http.post(`api/leaveFeedback`, feedback)
  }


  getPopularTrips(): Observable<any> {
    return this.http.get(`api/popular`);
  }

  cancelBooking(journeyId: number, userId: number): Observable<any> {
    return this.http.delete(`api/journey/${journeyId}/${userId}`)
  }


}
