import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';

import { AppComponent } from './app.component';

import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';

import { AgmCoreModule, PolylineManager, GoogleMapsAPIWrapper } from '@agm/core';


import {
  MenuComponent,
  CompletedJourneysComponent,
  UpcomigJourneysComponent,
  JourneyDetailComponent,
  JourneysComponent,
  LoginComponent,
  RegisterComponent,
  CreateJourneyComponent,
  UserProfileComponent,
  DeleteJourneyDialog
} from './components';
import { AuthService, JourneyService, ProfileService } from './services';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AuthGuard } from './guards/auth.guard';
import { AuthInterceptor } from './auth.interceptor';
import { ProfileComponent } from './components/profile/profile.component';
import { MaterialModule } from './material.module';
import { LeaveFeedbackDialog } from './components/journey-detail/journey-detail.component';

registerLocaleData(localeRu, 'ru');
@NgModule({
  declarations: [
    AppComponent,
    JourneysComponent,
    JourneyDetailComponent,
    RegisterComponent,
    LoginComponent,
    CompletedJourneysComponent,
    UpcomigJourneysComponent,
    ProfileComponent,
    MenuComponent,
    CreateJourneyComponent,
    UserProfileComponent,
    DeleteJourneyDialog,
    LeaveFeedbackDialog,
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBQj1Cls1I4SrNA7KFywtkUmdKCBP5xzBQ',
      libraries: ['places'],
      language:'ru-RU',
      region:"ru-RU"
    }),
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgProgressModule,

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true },
    JourneyService,
    AuthService,
    ProfileService,
    AuthGuard,
    PolylineManager,
    GoogleMapsAPIWrapper

  ],
  entryComponents: [DeleteJourneyDialog, LeaveFeedbackDialog],
  bootstrap: [AppComponent]
})
export class AppModule {
}
