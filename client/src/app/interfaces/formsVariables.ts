export let Genders = [
    { value: 'f', viewValue: 'Female' },
    { value: 'm', viewValue: 'Male' }
]

export let Years = [];
let Year = new Date(Date.now()).getFullYear();
for (let year = Year - 100; year <= Year - 17; year++)
    Years.push(year);


