export interface IJourney{

    journeyid:Number;
    dr_name: String;
    dr_surname:String ;
    modelofauto: String ;
    arrivalpnt: String;
    departurepnt: String;
    price_trip ;
    arr_time;
    dep_time;
    places;
}