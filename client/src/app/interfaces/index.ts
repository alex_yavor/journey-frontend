export interface journey {
    dr_name: string,
    dr_surname: string,
    age: Date,
    journeyid: number,
    dep_time: number,
    departure_point: string,
    departure_street: string,
    arrival_point: string,
    arrival_street: string,
    curplaces: number,
    places: number,
    arrival: Date,
    price: string
}


export interface passanger {
    name: string,
    surname: string,
    age: number,
    userid: number
}

export interface journeyDetail {
    departurepoint: string,
    arrivalpoint: string,
    departurestreet?: string,
    arrivalstreet?: string,
    departure: string,
    arrival: string,
    currseats: number,
    numofseats: number,
    price: string,
    userid: number,
    name: string,
    surname: string
    age: Date,
    phone: string,
    mark?: string,
    model?: string,
    color?: string,
    iscompleted: boolean
}

export interface profile {
    dname: string,
    dsurname: string,
    dage: number,
    trip: string,
    cgoodmark: number,
    cbadmarks: number,
    markauto: string,
    modelauto: string,
    colorauto: string
}

export interface feedback {
    name: string,
    surname: string,
    date_part: Date,
    mark: number,
    fedback: string
}